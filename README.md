# SQLKit
Welcome to the SQLKit Gitlab neighbourhood! This project is aimed to people that
want a lightweight library to generate SQL queries. It provides a fluent and
fast API to make your applications run faster without the overhead to of an object
relational mapper. With this library, you can use your favourite driver and all
its features.

# Getting Started
By default, this library generates generic SQL queries without configuration. You
only have to integrate it to your Swift project Package.swift file and import it.
It's all there is to it... really!

```swift
import SQLKit
```

# Quick Start
```swift
let select = Select().from("MyTable")
print(select)
// SELECT * FROM MyTable
```

# How to use
See the [wiki](https://gitlab.com/krazdax5/SQLKit/wikis/home) to know how to use SQLKit. There's a lot of examples and
explanations.

# Support, questions, more recent news
Need quick support? Want to be notified when the latest version comes out? Have
some questions to the team? Please, check out our [Twitter](https://twitter.com/SwiftSQLKit)!
