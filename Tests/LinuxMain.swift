import XCTest
@testable import SQLKitTests

XCTMain([
     testCase(SelectTests.allTests),
     testCase(ExpressionTests.allTests),
     testCase(SQLFormatterTests.allTests)
])
