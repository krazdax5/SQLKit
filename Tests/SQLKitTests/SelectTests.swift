import XCTest
@testable import SQLKit

class SelectTests: XCTestCase {
    func testAllColumnsNoTableName() {
        let select = Select()
        XCTAssertEqual(select.description, "SELECT * FROM")
    }

    func testAllColumns() {
        let select = Select() 
            .from("MyTable")
        let selectName = Select().from(N("MyTable"))
        let expectedSelect = "SELECT * FROM MyTable"
        
        XCTAssertEqual(select.description, expectedSelect)
        XCTAssertEqual(selectName.description, expectedSelect)
    }
    
    func testSomeColumnsNoTableName() {
        let select = Select("col1", "col2", "col3")
        let selectNames = Select(N("col1"), N("col2"), N("col3"))
        let expectedSelect = "SELECT col1, col2, col3 FROM"
        
        XCTAssertEqual(select.description, expectedSelect)
        XCTAssertEqual(selectNames.description, expectedSelect)
    }
    
    func testSomeColumns() {
        let select = Select("col1", "col2", "col3")
            .from("MyTable")
        let selectName = Select(N("col1"), N("col2"), N("col3"))
            .from(N("MyTable"))
        let expectedValue = "SELECT col1, col2, col3 FROM MyTable"
        
        XCTAssertEqual(select.description, expectedValue)
        XCTAssertEqual(selectName.description, expectedValue)
    }
    
    func testAliases() {
        let selectName = Select(N("col1", as: "c1"), N("col2", as: "c2"), N("col3", as: "c3"))
            .from(N("MyTable", as: "t1"))
        let select = Select("col1 AS c1", "col2 AS c2", "col3 AS c3").from("MyTable AS t1")
        let expectedValue = "SELECT col1 AS c1, col2 AS c2, col3 AS c3 FROM MyTable AS t1"
        
        XCTAssertEqual(select.description, expectedValue)
        XCTAssertEqual(selectName.description, expectedValue)
    }

    func testDistinct() {
        let select = Select().distinct().from("MyTable")
        XCTAssertEqual(select.description, "SELECT DISTINCT * FROM MyTable")
    }
    
    func testSimpleWhere() {
        let select = Select().from("MyTable")
            .where(Expression(left: "test", operator: .EQUAL, right: "test"))
        XCTAssertEqual(select.description, "SELECT * FROM MyTable WHERE test = 'test'")
    }
    
    func testComplexWhere() {
        let select = Select().from("MyTable")
            .where(|Expression(
                left: Expression(left: "test", operator: .EQUAL, right: 34),
                operator: .AND,
                right: Expression(left: "hola", operator: .GREATER_THAN, right: 56)
            ))
        XCTAssertEqual(select.description, "SELECT * FROM MyTable WHERE (test = 34 AND hola > 56)")
    }
    
    func testComplexWhereWithOperatorAnd() {
        let select = Select()
            .from("MyTable")
            .where(OP("test") == OP(34) && OP("hola") > OP(56.9))
        let expected = "SELECT * FROM MyTable WHERE test = 34 AND hola > 56.9"
        
        XCTAssertEqual(select.description, expected)
    }
    
    func testComplexWhereWithOperatorOr() {
        let select = Select().from("MyTable")
            .where(
                Expression(left: "test", operator: .LOWER_THAN, right: "lol") ||
                Expression(left: "test2", operator: .GREATER_THAN_OR_EQUAL, right: 50)
            )
        XCTAssertEqual(select.description, "SELECT * FROM MyTable WHERE test < 'lol' OR test2 >= 50")
    }
    
    func testBasicJoin() {
        let select = Select()
            .from("TestTable")
            .join("Table1", on: OP("Table1.col2") == OP("TestTable.joinTable1Col"))
            .where(OP("Table1.col3") >= OP(45))
        let expected = "SELECT * FROM TestTable JOIN Table1 ON Table1.col2 = TestTable.joinTable1Col WHERE Table1.col3 >= 45"
        
        XCTAssertEqual(select.description, expected)
    }
    
    func testInnerJoin() {
        let select = Select()
            .from("TestTable")
            .innerJoin("Table1", on: OP("Table1.col2") == OP("TestTable.joinTable1Col"))
            .where(OP("Table1.col3") >= OP(45))
        let expected = "SELECT * FROM TestTable INNER JOIN Table1 ON Table1.col2 = TestTable.joinTable1Col WHERE Table1.col3 >= 45"
        
        XCTAssertEqual(select.description, expected)
    }
    
    func testLeftJoin() {
        let select = Select()
            .from("TestTable")
            .leftJoin("Table1", on: OP("Table1.col2") == OP("TestTable.joinTable1Col"))
            .where(OP("Table1.col3") >= OP(45))
        let expected = "SELECT * FROM TestTable LEFT JOIN Table1 ON Table1.col2 = TestTable.joinTable1Col WHERE Table1.col3 >= 45"
        
        XCTAssertEqual(select.description, expected)
    }
    
    func testLeftOuterJoin() {
        let select = Select()
            .from("TestTable")
            .leftOuterJoin("Table1", on: OP("Table1.col2") == OP("TestTable.joinTable1Col"))
            .where(OP("Table1.col3") >= OP(45))
        let expected = "SELECT * FROM TestTable LEFT OUTER JOIN Table1 ON Table1.col2 = TestTable.joinTable1Col WHERE Table1.col3 >= 45"
        
        XCTAssertEqual(select.description, expected)
    }
    
    func testRightJoin() {
        let select = Select()
            .from("TestTable")
            .rightJoin("Table1", on: OP("Table1.col2") == OP("TestTable.joinTable1Col"))
            .where(OP("Table1.col3") >= OP(45))
        let expected = "SELECT * FROM TestTable RIGHT JOIN Table1 ON Table1.col2 = TestTable.joinTable1Col WHERE Table1.col3 >= 45"
        
        XCTAssertEqual(select.description, expected)
    }
    
    func testRightOuterJoin() {
        let select = Select()
            .from("TestTable")
            .rightOuterJoin("Table1", on: OP("Table1.col2") == OP("TestTable.joinTable1Col"))
            .where(OP("Table1.col3") >= OP(45))
        let expected = "SELECT * FROM TestTable RIGHT OUTER JOIN Table1 ON Table1.col2 = TestTable.joinTable1Col WHERE Table1.col3 >= 45"
        
        XCTAssertEqual(select.description, expected)
    }
    
    func testFullJoin() {
        let select = Select()
            .from("TestTable")
            .fullJoin("Table1", on: OP("Table1.col2") == OP("TestTable.joinTable1Col"))
            .where(OP("Table1.col3") >= OP(45))
        let expected = "SELECT * FROM TestTable FULL JOIN Table1 ON Table1.col2 = TestTable.joinTable1Col WHERE Table1.col3 >= 45"
        
        XCTAssertEqual(select.description, expected)
    }
    
    func testFullOuterJoin() {
        let select = Select()
            .from("TestTable")
            .fullOuterJoin("Table1", on: OP("Table1.col2") == OP("TestTable.joinTable1Col"))
            .where(OP("Table1.col3") >= OP(45))
        let expected = "SELECT * FROM TestTable FULL OUTER JOIN Table1 ON Table1.col2 = TestTable.joinTable1Col WHERE Table1.col3 >= 45"
        
        XCTAssertEqual(select.description, expected)
    }
    
    func testUnion() {
        let select = Select(N("col1"), N("col2", as: "test"))
            .from("TestTable")
            .where(OP("test")-!?)
            .union(
                Select("col2", "col4")
                .from("Table2")
                .where(OP("col2") > OP(45.6))
            )
        let expected = "SELECT col1, col2 AS test FROM TestTable WHERE test IS NOT NULL UNION SELECT col2, col4 FROM Table2 WHERE col2 > 45.6"
        
        XCTAssertEqual(select.description, expected)
    }
    
    func testUnionAll() {
        let select = Select(N("col1"), N("col2", as: "test"))
            .from("TestTable")
            .where(OP("test")-!?)
            .unionAll(
                Select("col2", "col4")
                .from("Table2")
                .where(OP("col2") > OP(45.6))
            )
        let expected = "SELECT col1, col2 AS test FROM TestTable WHERE test IS NOT NULL UNION ALL SELECT col2, col4 FROM Table2 WHERE col2 > 45.6"
        
        XCTAssertEqual(select.description, expected)
    }
    
    func testTop() {
        let select = Select()
            .top(100)
            .from("table1")
            .where(OP("col1") <= OP(12))
        let expected = "SELECT TOP 100 * FROM table1 WHERE col1 <= 12"
        
        XCTAssertEqual(select.description, expected)
    }
    
    func testTopPercent() {
        let select = try! Select()
            .top(percent: 15)
            .from("table1")
            .where(OP("col1") <= OP(12))
        let expected = "SELECT TOP 15 PERCENT * FROM table1 WHERE col1 <= 12"
        
        XCTAssertEqual(select.description, expected)
    }
    
    func testTopPercentFailLow() {
        XCTAssertThrowsError(
            try Select()
                .top(percent: -12)
                .from("table1")
                .where(OP("col1") <= OP(12))
        ) {error in
            XCTAssertEqual(error as? Select.SelectError, Select.SelectError.TopPercentRangeError)
        }
    }
    
    func testTopPercentFailHigh() {
        XCTAssertThrowsError(
            try Select()
                .top(percent: 112)
                .from("table1")
                .where(OP("col1") <= OP(12))
        ) {error in
            XCTAssertEqual(error as? Select.SelectError, Select.SelectError.TopPercentRangeError)
        }
    }

    func testOrderBy() {
        let select = Select()
            .from("table1")
            .orderBy("col1", "col2")
        let expected = "SELECT * FROM table1 ORDER BY col1, col2"

        XCTAssertEqual(select.description, expected)
    }

    func testOrderBySameNumberOfOrders() {
        let select = Select()
            .from("table1")
            .orderBy("col1", "col2", "col3", orders: [.ASCENDING, .ASCENDING, .DESCENDING])
        let expected = "SELECT * FROM table1 ORDER BY col1 ASC, col2 ASC, col3 DESC"

        XCTAssertEqual(select.description, expected)
    }

    func testOrderByFewerNumberOfOrders() {
        let select = Select()
            .from("table1")
            .orderBy("col1", "col2", "col3", orders: [.ASCENDING, .DESCENDING])
        let expected = "SELECT * FROM table1 ORDER BY col1 ASC, col2 DESC, col3"

        XCTAssertEqual(select.description, expected)
    }

    func testOrderByHigherNumberOfOrders() {
        let select = Select()
                .from("table1")
                .orderBy("col1", "col2", "col3", orders: [.ASCENDING, .DESCENDING, .ASCENDING, .DESCENDING])
        let expected = "SELECT * FROM table1 ORDER BY col1 ASC, col2 DESC, col3 ASC"

        XCTAssertEqual(select.description, expected)
    }
    
    func testCorrectOrderByWithUnion() {
        let select = Select(N("Col", as: "c")).from(N("Table", as: "t"))
            .union(
                Select().from("OneColTable")
                    .where(OP("ID")-!?)
            )
            .orderBy("col1", "col2")
        let expected = "SELECT Col AS c FROM Table AS t UNION SELECT * FROM OneColTable WHERE ID IS NOT NULL ORDER BY col1, col2"
        
        XCTAssertEqual(select.description, expected)
    }

    static var allTests : [(String, (SelectTests) -> () throws -> Void)] {
        return [
            ("testAllColumnsNoTableName", testAllColumnsNoTableName),
            ("testAllColumns", testAllColumns),
            ("testSomeColumnsNoTableName", testSomeColumnsNoTableName),
            ("testSomeColumns", testSomeColumns),
            ("testDistinct", testDistinct),
            ("testSimpleWhere", testSimpleWhere),
            ("testComplexWhere", testComplexWhere),
            ("testComplexWhereWithOperator", testComplexWhereWithOperatorAnd),
            ("testComplexWhereWithOperatorOr", testComplexWhereWithOperatorOr),
            ("testAliases", testAliases),
            ("testBasicJoin", testBasicJoin),
            ("testInnerJoin", testInnerJoin),
            ("testLeftJoin", testLeftJoin),
            ("testLeftOuterJoin", testLeftOuterJoin),
            ("testRightJoin", testRightJoin),
            ("testRightOuterJoin", testRightOuterJoin),
            ("testFullJoin", testFullJoin),
            ("testFullOuterJoin", testFullOuterJoin),
            ("testUnion", testUnion),
            ("testUnionAll", testUnionAll),
            ("testTop", testTop),
            ("testTopPercent", testTopPercent),
            ("testTopPercentFailLow", testTopPercentFailLow),
            ("testTopPercentFailHigh", testTopPercentFailHigh),
            ("testOrderBy", testOrderBy),
            ("testOrderBySameNumberOfOrders", testOrderBySameNumberOfOrders),
            ("testOrderByFewerNumberOfOrders", testOrderByFewerNumberOfOrders),
            ("testOrderByHigherNumberOfOrders", testOrderByHigherNumberOfOrders),
            ("testCorrectOrderByWithUnion", testCorrectOrderByWithUnion)
        ]
    }
}
