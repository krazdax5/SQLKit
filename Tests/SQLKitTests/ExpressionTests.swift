//
//  ExpressionTests.swift
//  SQLKit
//
//  Created by Charles Levesque on 2016-10-09.
//
//

import XCTest
@testable import SQLKit

class ExpressionTests : XCTestCase {
    func testSimpleExpressionFromOperands() {
        let expression = |Operand("test") == |Operand(26)
        let shortExpression = |(OP("test") == OP(26))
        let expressionCheck = |Expression(left: "test", operator: .EQUAL, right: 26)
        let stringExpression = "(test = 26)"
        
        XCTAssertEqual(expression.description, expressionCheck.description)
        XCTAssertEqual(expression.description, shortExpression.description)
        XCTAssertEqual(expression.description, stringExpression)
    }
    
    func testComplexExpressionFromOperands() {
        let expression =
            (OP("test") <= OP("Value")) &&
            (OP("test2") >= OP(45.897))
        let expressionCheck =
            Expression(left: "test", operator: .LOWER_THAN_OR_EQUAL, right: "Value") &&
            Expression(left: "test2", operator: .GREATER_THAN_OR_EQUAL, right: 45.897)
        let stringExpression = "test <= 'Value' AND test2 >= 45.897"
        
        XCTAssertEqual(expression.description, expressionCheck.description)
        XCTAssertEqual(expression.description, stringExpression)
    }
    
    func testINOperator() {
        let expression = OP("test") |> [1, 2, 3]
        let expressionInString = OP("test") |> ["1", "2", "3"]
        let expressionCheck = Expression(left: "test", operator: .IN, right: [1, 2, 3])
        let expressionCheckInString = Expression(left: "test", operator: .IN, right: ["1", "2", "3"])
        let stringExpression = "test IN (1, 2, 3)"
        let stringExpressionInString = "test IN ('1', '2', '3')"

        XCTAssertEqual(expression.description, expressionCheck.description)
        XCTAssertEqual(expression.description, stringExpression)
        XCTAssertEqual(expressionInString.description, expressionCheckInString.description)
        XCTAssertEqual(expressionCheckInString.description, stringExpressionInString)
    }

    func testBetweenOperator() {
        let expression = OP("test") ... (OP(1) && OP(5))
        let expressionCheck = Expression(
            left: "test",
            operator: .BETWEEN,
            right: Expression(
                left: 1,
                operator: .AND,
                right: 5
            )
        )
        let stringExpression = "test BETWEEN 1 AND 5"

        XCTAssertEqual(expression.description, expressionCheck.description)
        XCTAssertEqual(expression.description, stringExpression)
    }
    
    func testIsOperator() {
        let expression = OP("test") ==== OP(OperandType.NULL)
        let expressionCheck = Expression(
            left: "test",
            operator: .IS,
            right: OperandType.NULL
        )
        let expressionOperand = OP("test")-??
        let expressionString = "test IS NULL"
        
        XCTAssertEqual(expression.description, expressionCheck.description)
        XCTAssertEqual(expressionCheck.description, expressionOperand.description)
        XCTAssertEqual(expressionOperand.description, expressionString)
    }
    
    func testIsNotOperator() {
        let expression = OP("test") !=== OP(OperandType.NULL)
        let expressionCheck = Expression(
            left: "test",
            operator: .IS_NOT,
            right: OperandType.NULL
        )
        let expressionOperand = OP("test")-!?
        let expressionString = "test IS NOT NULL"
        
        XCTAssertEqual(expression.description, expressionCheck.description)
        XCTAssertEqual(expressionCheck.description, expressionOperand.description)
        XCTAssertEqual(expressionOperand.description, expressionString)
    }
    
    func testLikeOperator() {
        let expression = OP("test") ~= OP("%test")
        let expressionCheck = Expression(
            left: "test",
            operator: .LIKE,
            right: "%test"
        )
        let expressionString = "test LIKE '%test'"
        
        XCTAssertEqual(expression.description, expressionCheck.description)
        XCTAssertEqual(expressionCheck.description, expressionString)
    }
    
    func testNotLikeOperator() {
        let expression = OP("test") ~!= OP("%test%")
        let expressionCheck = Expression(
            left: "test",
            operator: .NOT_LIKE,
            right: "%test%"
        )
        let expressionString = "test NOT LIKE '%test%'"
        
        XCTAssertEqual(expression.description, expressionCheck.description)
        XCTAssertEqual(expressionCheck.description, expressionString)
    }
    
    func testDescriptionWithoutQuotes() {
        let expression = OP("test") == OP("apple")
        let expected = "test = apple"
        
        expression.formatter.formatOptions.insert(.withoutQuotesOnString)
        XCTAssertEqual(expression.description, expected)
        XCTAssertEqual(expression.description, expected)  // Twice to make sure that the option doesn't get reset  (issue #26)
    }

    static var allTests: [(String, (ExpressionTests) -> () throws -> Void)] {
        return [
            ("testSimpleExpressionFromOperands", testSimpleExpressionFromOperands),
            ("testComplexExpressionFromOperands", testComplexExpressionFromOperands),
            ("testINOperator", testINOperator),
            ("testBetweenOperator", testBetweenOperator),
            ("testIsOperator", testIsOperator),
            ("testIsNotOperator", testIsNotOperator),
            ("testLikeOperator", testLikeOperator),
            ("testNotLikeOperator", testNotLikeOperator),
            ("testDescriptionWithoutQuotes", testDescriptionWithoutQuotes)
        ]
    }
}

