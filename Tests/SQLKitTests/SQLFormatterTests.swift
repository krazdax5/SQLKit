//
//  SQLFormatterTests.swift
//  SQLKit
//
//  Created by Charles Levesque on 2017-08-11.
//
//

import XCTest
@testable import SQLKit

class SQLFormatterTests : XCTestCase {
    private var sqlFormatter: SQLFormatter!
    
    override func setUp() {
        self.sqlFormatter = SQLFormatter()
    }
    
    func testDate() {
        // 27th of October, 1991 at 2h07 and 10 seconds
        let calendar = Calendar(identifier: .gregorian)
        var dateComponents = DateComponents(calendar: calendar)
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "''yyyy-MM-dd'T'HH:mm:ss''"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateComponents.year = 1991
        dateComponents.month = 27
        dateComponents.day = 27
        dateComponents.hour = 2
        dateComponents.minute = 7
        dateComponents.second = 10
        
        let date = dateComponents.date!
        let defaultFormatterDate = dateFormatter.string(from: date)
        let sqlFormatterDate = self.sqlFormatter.string(for: date)!
        
        XCTAssertEqual(sqlFormatterDate, defaultFormatterDate)
    }
    
    func testString() {
        let test = "that string"
        let expected = "'that string'"
        
        XCTAssertEqual(expected, self.sqlFormatter.string(for: test)!)
        
        self.sqlFormatter.formatOptions.insert(.withoutQuotesOnString)
        
        XCTAssertEqual(test, self.sqlFormatter.string(for: test)!)
    }
    
    func testArray() {
    }
    
    static var allTests: [(String, (SQLFormatterTests) -> () throws -> Void)] {
        return [
            ("testDate", testDate),
            ("testString", testString),
            ("testArray", testArray)
        ]
    }
}
