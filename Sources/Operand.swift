//
//  Operand.swift
//  SQLKit
//
//  Created by Charles Levesque on 2016-10-08.
//
//

import Foundation

/// Represents an operand in an expression.
public struct Operand {
    public let value: CustomStringConvertible
    public var shouldAddParenthesis: Bool = false

    /// Instanciates a new Operand.
    ///
    /// - Parameter value: The value of the operand.
    /// - Parameter shouldAddParenthesis: Indicates whether there should be a parenthesis with this operand. Default false.
    public init<T: CustomStringConvertible>(_ value: T, shouldAddParenthesis: Bool = false) {
        self.value = value
        self.shouldAddParenthesis = shouldAddParenthesis
    }
}

infix operator |> : AdditionPrecedence
infix operator ==== : ComparisonPrecedence
infix operator !=== : ComparisonPrecedence
infix operator ~!= : ComparisonPrecedence
postfix operator -??
postfix operator -!?

public typealias OP = Operand

extension Operand {
    /// Overload of == to construct a new expression. The operator will be .EQUAL.
    ///
    /// - Parameters:
    ///   - left: The left part of the expression.
    ///   - right: The right part of the expression.
    /// - Returns: A new expression.
    public static func ==(left: Operand, right: Operand) -> Expression<Operand, Operand> {
        return Expression(
            left: left,
            operator: .EQUAL,
            right: right,
            shouldAddOpenningParenthesis: left.shouldAddParenthesis,
            shouldAddClosingParenthesis: right.shouldAddParenthesis
        )
    }

    /// Overload of != to construct a new expression. The operator will
    /// be .NOT_EQUAL.
    ///
    /// - Parameters:
    ///   - left: The left part of the expression.
    ///   - right: The right part of the expression.
    /// - Returns: A new expression.
    public static func !=(left: Operand, right: Operand) -> Expression<Operand, Operand> {
        return Expression(
            left: left,
            operator: .NOT_EQUAL,
            right: right,
            shouldAddOpenningParenthesis: left.shouldAddParenthesis,
            shouldAddClosingParenthesis: right.shouldAddParenthesis
        )
    }

    /// Overload of >= to construct a new expression. The operator will be
    /// .GREATER_THAN_OR_EQUAL.
    ///
    /// - Parameters:
    ///   - left: The left part of the expression.
    ///   - right: The right part of the expression.
    /// - Returns: A new expression.
    public static func >=(left: Operand, right: Operand) -> Expression<Operand, Operand> {
        return Expression(
            left: left,
            operator: .GREATER_THAN_OR_EQUAL,
            right: right,
            shouldAddOpenningParenthesis: left.shouldAddParenthesis,
            shouldAddClosingParenthesis: right.shouldAddParenthesis
        )
    }

    /// Overload of > to construct a new expression. The operator will be
    /// .GREATER_THAN.
    ///
    /// - Parameters:
    ///   - left: The left part of the expression.
    ///   - right: The right part of the expression.
    /// - Returns: A new expression.
    public static func >(left: Operand, right: Operand) -> Expression<Operand, Operand> {
        return Expression(
            left: left,
            operator: .GREATER_THAN,
            right: right,
            shouldAddOpenningParenthesis: left.shouldAddParenthesis,
            shouldAddClosingParenthesis: right.shouldAddParenthesis
        )
    }

    /// Overload of <= to construct a new expression. The operator will be
    /// .LOWER_THAN_OR_EQUAL.
    ///
    /// - Parameters:
    ///   - left: The left part of the expression.
    ///   - right: The right part of the expression.
    /// - Returns: A new expression.
    public static func <=(left: Operand, right: Operand) -> Expression<Operand, Operand> {
        return Expression(
            left: left,
            operator: .LOWER_THAN_OR_EQUAL,
            right: right,
            shouldAddOpenningParenthesis: left.shouldAddParenthesis,
            shouldAddClosingParenthesis: right.shouldAddParenthesis
        )
    }

    /// Overload of < to construct a new expression. The operator will be
    /// .LOWER_THAN.
    ///
    /// - Parameters:
    ///   - left: The left part of the expression.
    ///   - right: The right part of the expression.
    /// - Returns: A new expression.
    public static func <(left: Operand, right: Operand) -> Expression<Operand, Operand> {
        return Expression(
            left: left,
            operator: .LOWER_THAN,
            right: right,
            shouldAddOpenningParenthesis: left.shouldAddParenthesis,
            shouldAddClosingParenthesis: right.shouldAddParenthesis
        )
    }

    /// Overload of && to construct a new expression. The operator will be
    /// .AND.
    ///
    /// - Parameters:
    ///   - left: The left part of the expression.
    ///   - right: The right part of the expression.
    /// - Returns: A new expression.
    public static func &&(left: Operand, right: Operand) -> Expression<Operand, Operand> {
        return Expression(
            left: left,
            operator: .AND,
            right: right,
            shouldAddOpenningParenthesis: left.shouldAddParenthesis,
            shouldAddClosingParenthesis: right.shouldAddParenthesis
        )
    }

    /// Overload of || to construct a new expression. The operator will be
    /// .OR.
    ///
    /// - Parameters:
    ///   - left: The left part of the expression.
    ///   - right: The right part of the expression.
    /// - Returns: A new expression.
    public static func ||(left: Operand, right: Operand) -> Expression<Operand, Operand> {
        return Expression(
            left: left,
            operator: .OR,
            right: right,
            shouldAddOpenningParenthesis: left.shouldAddParenthesis,
            shouldAddClosingParenthesis: right.shouldAddParenthesis
        )
    }
    
    /// Overload of === to construct a new expression. The operator will be
    /// .IS.
    ///
    /// - Parameters:
    ///   - left: The left part of the expression.
    ///   - right: The right part of the expression.
    /// - Returns: A new expression.
    public static func ====(left: Operand, right: Operand) -> Expression<Operand, Operand> {
        return Expression(
            left: left,
            operator: .IS,
            right: right,
            shouldAddOpenningParenthesis: left.shouldAddParenthesis,
            shouldAddClosingParenthesis: right.shouldAddParenthesis
        )
    }

    /// Overload of !== to construct a new expression. The operator will be
    /// .IS_NOT.
    ///
    /// - Parameters:
    ///   - left: The left part of the expression.
    ///   - right: The right part of the expression.
    /// - Returns: A new expression.
    public static func !===(left: Operand, right: Operand) -> Expression<Operand, Operand> {
        return Expression(
            left: left,
            operator: .IS_NOT,
            right: right,
            shouldAddOpenningParenthesis: left.shouldAddParenthesis,
            shouldAddClosingParenthesis: right.shouldAddParenthesis
        )
    }

    /// Overload of |> to construct a new expression. The operator will
    /// be .IN.
    ///
    /// - Parameters:
    ///   - left: The left part of the expression.
    ///   - right: The right part of the expression which is a tuple.
    /// - Returns: A new expression.
    public static func |><U: CustomStringConvertible>(left: Operand, right: [U]) -> Expression<Operand, [U]> {
        return Expression(left: left, operator: .IN, right: right, shouldAddOpenningParenthesis: left.shouldAddParenthesis)
    }

    /// Overload of ... to build a new expression. The operator will be .BETWEEN.
    ///
    /// - Parameters:
    ///   - left: The operand for which to evaluate the range.
    ///   - right: The expression of the BETWEEN operator.
    /// - Returns: The BETWEEN complete expression.
    public static func ...<T, U>(left: Operand, right: Expression<T, U>)
        -> Expression<Operand, Expression<T, U>> {
            return Expression(left: left, operator: .BETWEEN, right: right, shouldAddOpenningParenthesis: left.shouldAddParenthesis)
    }
    
    /// Overload of ~= to construct a new expression. The operator will be
    /// .LIKE.
    ///
    /// - Parameters:
    ///   - left: The left part of the expression.
    ///   - right: The right part of the expression.
    /// - Returns: A new expression.
    public static func ~=(left: Operand, right: Operand) -> Expression<Operand, Operand> {
        return Expression(
            left: left,
            operator: .LIKE,
            right: right,
            shouldAddOpenningParenthesis: left.shouldAddParenthesis,
            shouldAddClosingParenthesis: right.shouldAddParenthesis
        )
    }

    /// Overload of ~= to construct a new expression. The operator will be
    /// .NOT_LIKE.
    ///
    /// - Parameters:
    ///   - left: The left part of the expression.
    ///   - right: The right part of the expression.
    /// - Returns: A new expression.
    public static func ~!=(left: Operand, right: Operand) -> Expression<Operand, Operand> {
        return Expression(
            left: left,
            operator: .NOT_LIKE,
            right: right,
            shouldAddOpenningParenthesis: left.shouldAddParenthesis,
            shouldAddClosingParenthesis: right.shouldAddParenthesis
        )
    }

    /// Overload of the | operator. Adds a parenthesis before of after (depending on precedence)
    /// the operand.
    ///
    /// - Parameter right: The operand to add a parenthesis.
    /// - Returns: The modified operand.
    public static prefix func |(right: Operand) -> Operand {
        var copy = right
        copy.shouldAddParenthesis = true
        return copy
    }

    /// Overload of the * operator. Generates a new null-checking expression. This is
    /// IS NULL.
    ///
    /// - Parameter left: The operand to null-check against.
    /// - Returns: A new expression.
    public static postfix func -??(left: Operand) -> Expression<Operand, OperandType> {
        return Expression(left: left, operator: .IS, right: OperandType.NULL)
    }

    /// Overload of the !* operator. Generates a new null-checking expression. This is
    /// IS NOT NULL.
    ///
    /// - Parameter left: The operand to null-check against.
    /// - Returns: A new expression.
    public static postfix func -!?(left: Operand) -> Expression<Operand, OperandType> {
        return Expression(left: left, operator: .IS_NOT, right: OperandType.NULL)
    }
}
