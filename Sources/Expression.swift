//
//  Expression.swift
//  SQLKit
//
//  Created by Charles Levesque on 2016-10-02.
//
//

import Foundation

/// Represents a printable SQL expression.
open class Expression<LeftOperandType, RightOperandType>
    : CustomStringConvertible {
    private let left: LeftOperandType
    private let right: RightOperandType
    private let `operator`: Operator

    /// Indicates whether the expression should add an openning parenthesis.
    open var shouldAddOpenningParenthesis: Bool
    /// Indicates whether the expression should add a closing parenthesis.
    open var shouldAddClosingParenthesis: Bool
    /// The formatter used to convert Swift primitives and objects into SQL types and clauses.
    open var formatter = SQLFormatter()

    /// Creates a new instance of an expression.
    ///
    /// - Parameters:
    ///   - leftOperand: The left operand of the expression.
    ///   - operator: The operator of the expression.
    ///   - rightOperand: The right operand of the expression.
    ///   - shouldAddOpenningParenthesis: Adds or not an openning parenthesis before the expression. Default false.
    ///   - shouldAddClosingParenthesis: Adds or not a closing parenthesis after the expression. Default false.
    public init(
        left leftOperand: LeftOperandType,
        `operator`: Operator,
        right rightOperand: RightOperandType,
        shouldAddOpenningParenthesis: Bool = false,
        shouldAddClosingParenthesis: Bool = false
    ) {
        self.left = leftOperand
        self.right = rightOperand
        self.operator = `operator`
        self.shouldAddOpenningParenthesis = shouldAddOpenningParenthesis
        self.shouldAddClosingParenthesis = shouldAddClosingParenthesis
    }

    public var description: String {
        let right: String
        let left: String
        
        if let rightOperand = self.right as? Operand {
            right = self.formatter.string(for: rightOperand.value)!
        } else {
            right = self.formatter.string(for: self.right)!
        }
        
        if let leftOperand = self.left as? Operand {
            let originalOptionsContainsWithoutQuotesOption = self.formatter.formatOptions.contains(.withoutQuotesOnString)
            
            if leftOperand.value is String && !originalOptionsContainsWithoutQuotesOption {
                self.formatter.formatOptions.insert(.withoutQuotesOnString)
            }
            
            left = self.formatter.string(for: leftOperand.value)!
            
            if !originalOptionsContainsWithoutQuotesOption {
                self.formatter.formatOptions.remove(.withoutQuotesOnString)
            }
        } else if self.left is String {
            let originalOptionsContainsWithoutQuotesOption = self.formatter.formatOptions.contains(.withoutQuotesOnString)
            
            if !originalOptionsContainsWithoutQuotesOption {
                self.formatter.formatOptions.insert(.withoutQuotesOnString)
            }
            
            left = self.formatter.string(for: self.left)!
            
            if !originalOptionsContainsWithoutQuotesOption {
                self.formatter.formatOptions.remove(.withoutQuotesOnString)
            }
        } else {
            left = self.formatter.string(for: self.left)!
        }
        
        var description = "\(left) \(self.operator) \(right)"

        if self.shouldAddOpenningParenthesis {
            description = "(" + description
        }

        if self.shouldAddClosingParenthesis {
            description += ")"
        }
        
        return description
    }
}

prefix operator |

extension Expression {
    /// Overload of && to make an expression of expressions with less verbosity and more clarity.
    /// Makes a logical AND in the parent expression.
    ///
    /// - Parameters:
    ///   - left: The left expression.
    ///   - right: The right expression.
    /// - Returns: An expression that contains two other expression.
    open static func &&<T, U>(left: Expression<LeftOperandType, RightOperandType>, right: Expression<T, U>)
        -> Expression<Expression<LeftOperandType, RightOperandType>, Expression<T, U>> {
            return Expression<Expression<LeftOperandType, RightOperandType>, Expression<T, U>>(left: left, operator: .AND, right: right)
    }
    
    /// Overload of || to make an expression of expressions with less verbosity and more clarity.
    /// Makes a logical OR in the parent expression.
    ///
    /// - Parameters:
    ///   - left: The left expression.
    ///   - right: The right expression.
    /// - Returns: An expression that contains two other expression.
    open static func ||<T, U>(left: Expression<LeftOperandType, RightOperandType>, right: Expression<T, U>)
        -> Expression<Expression<LeftOperandType, RightOperandType>, Expression<T, U>> {
            return Expression<Expression<LeftOperandType, RightOperandType>, Expression<T, U>>(left: left, operator: .OR, right: right)
    }

    /// Overload of | as a prefix to add parenthesis around the expression.
    ///
    /// - Parameter right: The expression in which to add the parenthesis.
    /// - Returns: The expression with parenthesis.
    open static prefix func |(right: Expression<LeftOperandType, RightOperandType>) -> Expression<LeftOperandType, RightOperandType> {
        right.shouldAddOpenningParenthesis = true
        right.shouldAddClosingParenthesis = true
        return right
    }
}
