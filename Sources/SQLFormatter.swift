//
//  SQLFormatter.swift
//  SQLKit
//
//  Created by Charles Levesque on 2017-01-21.
//
//

import Foundation

extension SQLFormatter {
    /// Options for SQLFormatter.
    public struct Options: OptionSet {
        public let rawValue: UInt

        /// Do not put SQL quotes on strings
        public static let withoutQuotesOnString = Options(rawValue: 1 << 0)
        
        public init(rawValue: UInt) {
            self.rawValue = rawValue
        }
    }
}

/// Formatter from any kind of object to SQL.
open class SQLFormatter : Formatter {
    open var formatOptions: Options = []

    open override func string(for obj: Any?) -> String? {
        if let concreteObject = obj {
            if let string = concreteObject as? String {
                return self.format(string: string)
            }
            
            if let date = concreteObject as? Date {
                return self.format(date: date)
            }
            
            if let array = concreteObject as? [CustomStringConvertible] {
                return self.format(array: array)
            }

            if let customString = concreteObject as? CustomStringConvertible {
                return customString.description
            }
        }

        return nil
    }

    /// Format a string according to the options given to the formatter.
    ///
    /// - Parameter string: The string to format
    /// - Returns: A formatted string.
    private func format(string: String) -> String {
        if formatOptions.contains(.withoutQuotesOnString) {
            return string
        }

        return "'\(string)'"
    }
    
    /// Format a date for the database to be able to read and compare it.
    ///
    /// - Parameter date: The date to format.
    /// - Returns: The date formatted into a string.
    private func format(date: Date) -> String {
        let formatter: Formatter
        let stringFormat = self.formatOptions.contains(.withoutQuotesOnString) ? "''yyyy-MM-dd'T'HH:mm:ss''" : "yyyy-MM-dd'T'HH:mm:ss"
        let utcTimeZone = TimeZone(secondsFromGMT: 0)
        let string: String
        
        #if os(Linux)
            formatter = DateFormatter()
            (formatter as! DateFormatter).dateFormat = stringFormat
            (formatter as! DateFormatter).timeZone = utcTimeZone
            string = formatter.string(for: date)!
        #else
            if #available(OSX 10.12, iOS 10.0, tvOS 10.0, watchOS 3.0, *) {
                formatter = ISO8601DateFormatter()
                (formatter as! ISO8601DateFormatter).formatOptions = [
                    .withColonSeparatorInTime,
                    .withTime,
                    .withFullDate
                ]
                
                if self.formatOptions.contains(.withoutQuotesOnString) {
                    string = formatter.string(for: date)!
                } else {
                    string = "'\(formatter.string(for: date)!)'"
                }
            } else {
                formatter = DateFormatter()
                (formatter as! DateFormatter).dateFormat = stringFormat
                (formatter as! DateFormatter).timeZone = utcTimeZone
                string = formatter.string(for: date)!
            }
        #endif
        
        return string
    }
    
    /// Format an array into an SQL array as a string.
    ///
    /// - Parameter array: The array to format. Must be an array of CustomStringConvertible.
    /// - Returns: The array formatted into a string.
    private func format(array: [CustomStringConvertible]) -> String {
        if array is [String] {
            return array
                .description
                .replacingOccurrences(of: "\"", with: "'")
                .replacingOccurrences(of: "[", with: "(")
                .replacingOccurrences(of: "]", with: ")")
        } else {
            return array
                .description
                .replacingOccurrences(of: "[", with: "(")
                .replacingOccurrences(of: "]", with: ")")
        }
    }
}
