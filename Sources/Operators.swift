//
//  Operators.swift
//  SQLKit
//
//  Created by Charles Levesque on 2016-10-02.
//
//

import Foundation

/// Operators to use with expressions.
///
/// ```
/// case AND
/// case OR
/// case EQUAL
/// case NOT_EQUAL
/// case GREATER_THAN
/// case LOWER_THAN
/// case GREATER_THAN_OR_EQUAL
/// case LOWER_THAN_OR_EQUAL
/// case BETWEEN
/// case LIKE
/// case NOT_LIKE
/// case IN
/// case IS
/// case IS_NOT
/// ```
public enum Operator : String, CustomStringConvertible {
    /// The logical SQL AND operator.
    case AND = "AND"
    /// The logical SQL OR operator.
    case OR = "OR"
    /// The logical SQL equality ("=") operator.
    case EQUAL = "="
    /// The logical SQL inverse equality ("<>") operator.
    case NOT_EQUAL = "<>"
    /// The logical SQL greater than (">") operator.
    case GREATER_THAN = ">"
    /// The logical SQL lower than ("<") operator.
    case LOWER_THAN = "<"
    /// The logical SQL greater than or equal (">=") operator.
    case GREATER_THAN_OR_EQUAL = ">="
    /// The logical SQL lower than or equal ("<=") operator.
    case LOWER_THAN_OR_EQUAL = "<="
    /// The SQL BETWEEN operator.
    case BETWEEN = "BETWEEN"
    /// The SQL LIKE operator.
    case LIKE = "LIKE"
    /// The SQL NOT LIKE operator.
    case NOT_LIKE = "NOT LIKE"
    /// The SQL IN operator.
    case IN = "IN"
    /// The SQL IS operator.
    case IS = "IS"
    /// The SQL IS NOT operator.
    case IS_NOT = "IS NOT"
    
    public var description: String {
        get {
            return self.rawValue
        }
    }
}
