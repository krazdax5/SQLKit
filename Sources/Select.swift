//
//  Select.swift
//  SQLKit
//
//  Created by Charles Levesque on 2016-09-25.
//
//

import Foundation

/// Represents a SELECT SQL statement.
///
/// This class is designed to be fluent. Therefore, calls is encouraged to be
/// in the form of:
///
///     Select()
///         .distinct()
///         .from("mytable")
///         .join("table1", on: OP("table1.col1") == OP("mytable.col2"))
///         .where(...)
///
///
open class Select : CustomStringConvertible {
    private let columns: [Name]
    private var tableName: Name?
    private var isDistinct = false
    private var whereClause: String?
    private var union: Select?
    private var unionAll: Select?
    private var joins = [Join]()
    private var top: (maxRows: Int?, percent: Int?)?
    private var orderBys: [(column: String, order: GroupByOrder?)] = []
    
    /// Possible errors when constructing a select clause.
    ///
    /// ```
    /// case TopPercentRangeError
    /// ```
    public enum SelectError : Error {
        /// Thrown when the *percent* number from `top(percent:)` function is not between 0 and 100.
        case TopPercentRangeError
    }
    
    
    /// Possible orders applied to GROUP BY clauses.
    ///
    /// ```
    /// case ASCENDING
    /// case DESCENDING
    /// ```
    public enum GroupByOrder : String, CustomStringConvertible {
        case ASCENDING = "ASC"
        case DESCENDING = "DESC"
        
        public var description: String {
            return self.rawValue
        }
    }
    
    /// Used internally to represent a SQL JOIN clause.
    private struct Join : CustomStringConvertible {
        private let type: Type
        private let tableName: Name
        private let toJoinExpression: String
        
        /// Different types of SQL JOIN for SELECT statements.
        ///
        /// ```
        /// case DEFAULT
        /// case INNER
        /// case LEFT
        /// case LEFT_OUTER
        /// case RIGHT
        /// case RIGHT_OUTER
        /// case FULL
        /// case FULL_OUTER
        /// ```
        enum `Type` : String, CustomStringConvertible {
            /// The default JOIN clause
            case DEFAULT = "JOIN"
            /// INNER JOIN clause.
            case INNER = "INNER JOIN"
            /// LEFT JOIN clause.
            case LEFT = "LEFT JOIN"
            /// LEFT OUTER JOIN clause.
            case LEFT_OUTER = "LEFT OUTER JOIN"
            /// RIGHT JOIN clause.
            case RIGHT = "RIGHT JOIN"
            /// RIGHT OUTER JOIN clause.
            case RIGHT_OUTER = "RIGHT OUTER JOIN"
            /// FULL JOIN clause.
            case FULL = "FULL JOIN"
            /// FULL OUTER JOIN clause.
            case FULL_OUTER = "FULL OUTER JOIN"
            
            public var description: String {
                get {
                    return self.rawValue
                }
            }
        }
        
        /// Instantiate a new SQL JOIN.
        ///
        /// - Parameters:
        ///    - tableName: The name of the table to join.
        ///    - joinExpression: The expression that joins the two tables.
        ///    - type: The type of join. Default .DEFAULT ("JOIN").
        init<T, U>(
            table tableName: String,
            on joinExpression: Expression<T, U>,
            type: Type = .DEFAULT
        ) {
            self.init(table: N(tableName), on: joinExpression, type: type)
        }
        
        /// Instantiate a new SQL JOIN.
        ///
        /// - Parameters:
        ///    - tableName: The name of the table to join.
        ///    - joinExpression: The expression that joins the two tables.
        ///    - type: The type of join. Default .DEFAULT ("JOIN").
        init<T, U>(
            table tableName: Name,
            on joinExpression: Expression<T, U>,
            type: Type = .DEFAULT
        ) {
            joinExpression.formatter.formatOptions = .withoutQuotesOnString
            self.type = type
            self.tableName = tableName
            self.toJoinExpression = joinExpression.description
        }
        
        var description: String {
            return "\(self.type) \(self.tableName) ON \(self.toJoinExpression)";
        }
    }
    
    //MARK: Initialiazers
    
    /// Creates an instance of Select with all columns ("*").
    public init() {
        self.columns = [Name]()
    }
    
    /// Creates an instance of Select with strings as columns.
    ///
    /// Here you can specify columns you want to retrieve by passing their names.
    ///
    /// - Parameter columns: The name of the columns to retrieve.
    public init(_ columns: String...) {
        self.columns = columns.map {value in N(value)}
    }

    /// Creates an instance of Select with Names as columns
    ///
    /// - Parameter columns: The name of the columns to retrieve.
    public init(_ columns: Name...) {
        self.columns = columns
    }
    
    //MARK: FROM methods
    
    /// Add a table name to the query.
    ///
    /// - Parameter tableName: The name of the table where to do the query
    /// - Returns: Self
    public func from(_ tableName: Name) -> Select {
        self.tableName = tableName
        return self
    }

    //MARK: DISTINCT methods
    
    /// Indicates that the results should be distinct from one another.
    ///
    /// - Returns: Self
    public func distinct() -> Select {
        self.isDistinct = true
        return self
    }
    
    /// Add a maximum number of rows to return.
    ///
    /// - Parameter maxRows: Maximum number of rows to return.
    /// - Returns: Self
    public func top(_ maxRows: Int) -> Select {
        self.top = (maxRows, nil)
        return self
    }
    
    /// Add a maximum number of rows to return based on a percentage.
    ///
    /// - Parameter percent: The percentage of rows from the table to return.
    /// - Returns: Self
    /// - Throws: `SelectError.TopPercentRangeError`
    /// - Precondition: *percent* is between 0 and 100 inclusively
    public func top(percent: Int) throws -> Select {
        guard 0 <= percent && percent <= 100 else {
            throw SelectError.TopPercentRangeError
        }

        self.top = (nil, percent)
        return self
    }
    
    // MARK: JOIN methods
    
    /// Join SELECT to a table. Default join ("JOIN")
    ///
    /// - Parameters:
    ///   - tableName: The name of the table to join.
    ///   - joinExpression: The expression that joins the two tables.
    /// - Returns: Self
    public func join<T, U>(_ tableName: Name, on joinExpression: Expression<T, U>) -> Select {
        self.joins.append(Join(table: tableName, on: joinExpression))
        return self
    }
    
    /// Join SELECT to a table. Inner join ("INNER JOIN")
    ///
    /// - Parameters:
    ///   - tableName: The name of the table to join.
    ///   - joinExpression: The expression that joins the two tables.
    /// - Returns: Self
    public func innerJoin<T, U>(_ tableName: Name, on joinExpression: Expression<T, U>) -> Select {
        self.joins.append(Join(table: tableName, on: joinExpression, type: .INNER))
        return self
    }
    
    /// Join SELECT to a table. Left join ("LEFT JOIN")
    ///
    /// - Parameters:
    ///   - tableName: The name of the table to join.
    ///   - joinExpression: The expression that joins the two tables.
    /// - Returns: Self
    public func leftJoin<T, U>(_ tableName: Name, on joinExpression: Expression<T, U>) -> Select {
        self.joins.append(Join(table: tableName, on: joinExpression, type: .LEFT))
        return self
    }
    
    /// Join SELECT to a table. Left outer join ("LEFT OUTER JOIN")
    ///
    /// - Parameters:
    ///   - tableName: The name of the table to join.
    ///   - joinExpression: The expression that joins the two tables.
    /// - Returns: Self
    public func leftOuterJoin<T, U>(_ tableName: Name, on joinExpression: Expression<T, U>) -> Select {
        self.joins.append(Join(table: tableName, on: joinExpression, type: .LEFT_OUTER))
        return self
    }
    
    /// Join SELECT to a table. Right join ("RIGHT JOIN")
    ///
    /// - Parameters:
    ///   - tableName: The name of the table to join.
    ///   - joinExpression: The expression that joins the two tables.
    /// - Returns: Self
    public func rightJoin<T, U>(_ tableName: Name, on joinExpression: Expression<T, U>) -> Select {
        self.joins.append(Join(table: tableName, on: joinExpression, type: .RIGHT))
        return self
    }
    
    /// Join SELECT to a table. Right outer join ("RIGHT OUTER JOIN")
    ///
    /// - Parameters:
    ///   - tableName: The name of the table to join.
    ///   - joinExpression: The expression that joins the two tables.
    /// - Returns: Self
    public func rightOuterJoin<T, U>(_ tableName: Name, on joinExpression: Expression<T, U>) -> Select {
        self.joins.append(Join(table: tableName, on: joinExpression, type: .RIGHT_OUTER))
        return self
    }
    
    /// Join SELECT to a table. Full join ("FULL JOIN")
    ///
    /// - Parameters:
    ///   - tableName: The name of the table to join.
    ///   - joinExpression: The expression that joins the two tables.
    /// - Returns: Self
    public func fullJoin<T, U>(_ tableName: Name, on joinExpression: Expression<T, U>) -> Select {
        self.joins.append(Join(table: tableName, on: joinExpression, type: .FULL))
        return self
    }
    
    /// Join SELECT to a table. Full outer join ("FULL OUTER JOIN")
    ///
    /// - Parameters:
    ///   - tableName: The name of the table to join.
    ///   - joinExpression: The expression that joins the two tables.
    /// - Returns: Self
    public func fullOuterJoin<T, U>(_ tableName: Name, on joinExpression: Expression<T, U>) -> Select {
        self.joins.append(Join(table: tableName, on: joinExpression, type: .FULL_OUTER))
        return self
    }
    
    //MARK: WHERE methods
    
    /// Adds a where statement to the select.
    /// You can specify any type that in the expression that is CustomStringConvertible compliant.
    /// The class Expression is itself compliant to that protocol.
    ///
    /// - Parameter expression: The expression to evaluate as part of the "where".
    /// - Returns: Self
    public func `where`<T, U>(_ expression: Expression<T, U>) -> Select {
        self.whereClause = expression.description
        return self
    }
    
    /// Overload of where that will accept a complex Expression.
    ///
    /// - Parameter expression: A complex expression like (Expression & Expression)
    /// - Returns: Self
    public func `where`<T, U>(_ expression: Expression<Expression<T, U>, Expression<T, U>>) -> Select {
        self.whereClause = expression.description
        return self
    }
    
    //MARK: UNION methods
    
    /// Unifies the current select with the one passed as a parameter. (Adds UNION)
    ///
    /// - Parameter selectToUnify: The select to unify with.
    /// - Returns: Self
    public func union(_ selectToUnify: Select) -> Select {
        self.union = selectToUnify
        self.unionAll = nil
        return self
    }
    
    /// Unifies the current select with the one passed as a parameter. (Adds UNION ALL)
    ///
    /// - Parameter selectToUnify: The select to unify with.
    /// - Returns: Self
    public func unionAll(_ selectToUnify: Select) -> Select {
        self.union = nil
        self.unionAll = selectToUnify
        return self
    }
    
    /// Add a ORDER BY clause to the select.
    ///
    /// - Parameters:
    ///   - columns: The columns to order from
    ///   - orders: A list of ordering directions matching the order of the columns
    /// - Returns: Self
    public func orderBy(_ columns: String..., orders: [GroupByOrder]? = nil) -> Select {
        for index in 0..<columns.count {
            if let unwrappedOrders = orders, index < unwrappedOrders.count {
                self.orderBys.append((columns[index], unwrappedOrders[index]))
            } else {
                self.orderBys.append((columns[index], nil))
            }
        }
        return self
    }
    
    //MARK: Other methods
    
    open var description: String {
        var terms = [String]()
        let columns = self.columns.map {column in column.description}
        let columnsToQuery = self.columns.isEmpty ? "*" : columns.joined(separator: ", ")
        
        terms.append("SELECT")
        
        if self.isDistinct {
            terms.append("DISTINCT")
        }
        
        if let top = self.top, let maxRows = top.maxRows {
            terms.append("TOP \(maxRows)")
        } else if let top = self.top, let percent = top.percent {
            terms.append("TOP \(percent) PERCENT")
        }
        
        terms.append(columnsToQuery)
        terms.append("FROM")
        
        if let tableName = self.tableName {
            terms.append(tableName.description)
        }
        
        for join in self.joins {
            terms.append(join.description)
        }
        
        if let whereValue = self.whereClause {
            terms.append("WHERE")
            terms.append(whereValue)
        }
        
        if let union = self.union {
            terms.append("UNION")
            terms.append(union.description)
        } else if let unionAll = self.unionAll {
            terms.append("UNION ALL")
            terms.append(unionAll.description)
        }
        
        if self.orderBys.count > 0 {
            let strings = self.orderBys.reduce([String]()) { (stringList, currentOrder) -> [String] in
                var stringListMutable = stringList
                let term: String
                
                // Add a comma if needed
                if let orderByOrder = currentOrder.order {
                    term = "\(currentOrder.column) \(orderByOrder)"
                } else {
                    term = currentOrder.column
                }
                
                stringListMutable.append(term)
                
                return stringListMutable
            }
            
            terms.append("ORDER BY")
            terms.append(strings.joined(separator: ", "))
        }
        
        return terms.joined(separator: " ")
    }
}

extension Select {
    /// - Remark: Overload. See: from(_:)
    public func from(_ tableName: String) -> Select {
        return self.from(N(tableName))
    }
    
    /// - Remark: Overload. See: `join(_:on:)`
    public func join<T, U>(_ tableName: String, on joinExpression: Expression<T, U>) -> Select {
        return self.join(N(tableName), on: joinExpression)
    }
    
    /// - Remark: Overload. See: `innerJoin(_:on:)`
    public func innerJoin<T, U>(_ tableName: String, on joinExpression: Expression<T, U>) -> Select {
        return self.innerJoin(N(tableName), on: joinExpression)
    }
    
    /// - Remark: Overload. See: `leftJoin(_:on:)`
    public func leftJoin<T, U>(_ tableName: String, on joinExpression: Expression<T, U>) -> Select {
        return self.leftJoin(N(tableName), on: joinExpression)
    }
    
    /// - Remark: Overload. See: `leftOuterJoin(_:on:)`
    public func leftOuterJoin<T, U>(_ tableName: String, on joinExpression: Expression<T, U>) -> Select {
        return self.leftOuterJoin(N(tableName), on: joinExpression)
    }
    
    /// - Remark: Overload. See: `rightJoin(_:on:)`
    public func rightJoin<T, U>(_ tableName: String, on joinExpression: Expression<T, U>) -> Select {
        return self.rightJoin(N(tableName), on: joinExpression)
    }
    
    /// - Remark: Overload. See: `rightOuterJoin(_:on:)`
    public func rightOuterJoin<T, U>(_ tableName: String, on joinExpression: Expression<T, U>) -> Select {
        return self.rightOuterJoin(N(tableName), on: joinExpression)
    }
    
    /// - Remark: Overload. See: `fullJoin(_:on:)`
    public func fullJoin<T, U>(_ tableName: String, on joinExpression: Expression<T, U>) -> Select {
        return self.fullJoin(N(tableName), on: joinExpression)
    }
    
    /// - Remark: Overload. See: `fullOuterJoin(_:on:)`
    public func fullOuterJoin<T, U>(_ tableName: String, on joinExpression: Expression<T, U>) -> Select {
        return self.fullOuterJoin(N(tableName), on: joinExpression)
    }
}
