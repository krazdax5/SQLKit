//
//  OperandType.swift
//  SQLKit
//
//  Created by Charles Levesque on 2017-01-01.
//
//

import Foundation

/// Operand types for expressions.
///
/// ```
/// case NULL
/// ```
public enum OperandType : String, CustomStringConvertible {
    /// SQL NULL value.
    case NULL = "NULL"
    
    public var description: String {
        get {
            return self.rawValue
        }
    }
}
