//
// Name.swift
// SQLKit
//
// Created by Charles Levesque
// Contains the definition of a Name.
//

/// Represents a name that can be associated with an alias.
public struct Name: CustomStringConvertible {
    private let value: String
    private let alias: String?

    /// Creates a new instance of a name.
    ///
    /// - Parameters:
    ///   - value: value The value of the name.
    ///   - alias: The alias associated with the name.
    public init(_ value: String, `as` alias: String? = nil) {
        self.value = value
        self.alias = alias
    }

    public var description: String {
        get {
            if let alias = self.alias {
                return "\(self.value) AS \(alias)"
            }

            return self.value
        }
    }
}

public typealias N = Name
