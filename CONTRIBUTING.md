# Contribution

Want to contribute to the project? That's easy and there's a ton you can do. Helping
in this project will ensure that the project stays fast, easy to use, well
documented and easy to contribute. Continue to read to know what kind of work
you can do.

# How to contribute

* Fork the project

      By forking the project, you can do your own changes the way you like without
      impacting the current work done by the team of SQLKit. When you're done, just
      create a Merge Request and someone will review it with you. It's that simple.
      
      Be sure to include enough information about your changes in your request to
      help the reviewer catch what you're trying to add. It will be much faster that
      way. To help describing your changes, you could include a **description** of your
      changes, your **motivations**, which **modules** are impacted and which **issues**
      are you trying to resolve (if any).
  
* Pick an issue

      This one is the most simple to do. First you have to fork the project. Then,
      read the issue that interest you and add a comment to it saying that you'll try
      to do it and a deadline date to do it. The deadline is there to help you fix
      that objectif in time and to indicate to others how much time you think you need.
      That way, if you pass the deadline, someone can contact you to help you. That
      can lead to a little competition of course! Stay polite and everything will be
      just fine. If you have any questions, write a comment.
  
* Document the project

      Found a hole in the documentation? Want to clarify something? Have some opinions
      on how to explain that other thing? Write it! Then, you can open an issue on the
      subject. Someone from the team will review your documentation with you and eventually
      integrate it with a mention of you.
  
* Integrate the team

      Want ot take part in the decision process? Reviewing code is your hobby? Writing
      endless documentation is what defines you? Write an email to the team and discuss
      your motivations to get access.
  
## Recommendations and expertise are always welcome

Any piece of advice is welcome. We're very open minded. New ideas, optimizations
and new approaches can help the project in many ways. The team loves to learn
more and more. We're the first to admit that we don't know everything and we are
all humans so mistakes happens, unfortunately.
