//: Playground - noun: a place where people can play

import Cocoa
import SQLKit

// Basic SELECT from a table
let test = Select("TestCol").from("TestTable")

// Unify with another select
let testUnion =
    Select(N("Col", as: "c")).from(N("Table", as: "t"))
    .union(
        Select().from("OneColTable")
        .where(OP("ID")-!?)
    )
