//
//  SQLKitPlayground.h
//  SQLKitPlayground
//
//  Created by Charles Levesque on 2017-01-22.
//
//

#import <Cocoa/Cocoa.h>

//! Project version number for SQLKitPlayground.
FOUNDATION_EXPORT double SQLKitPlaygroundVersionNumber;

//! Project version string for SQLKitPlayground.
FOUNDATION_EXPORT const unsigned char SQLKitPlaygroundVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SQLKitPlayground/PublicHeader.h>


